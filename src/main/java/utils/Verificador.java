package utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.DataFormatException;

public class Verificador {

    /**
     * *
     * Valida el format d'un DNI
     *
     * @param nif
     * @return
     */
    public static boolean nif(String nif) {

        boolean ret = false;

        String regex = "^[0-9]{8}[A-Z]{1}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(nif);

        if (matcher.find() && checkNIFLetter(nif)) {
            ret = true;
        }

        return ret;
    }

    public static boolean checkNIFLetter(String nif) {
        char[] correspondingLetters = {'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'};
        int number = Integer.parseInt(nif.substring(0, 8));
        int index = number % 23;
        char letter = nif.charAt(8);

        return correspondingLetters[index] == letter;
    }

    /**
     * *
     * Valida un email amb un format correcte
     *
     * @param addr
     * @return
     */
    public static boolean email(String addr) {

        boolean ret = false;

        String expresion = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9]+)*@[A-Za-z0-9-]+(\\.[_A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pat = Pattern.compile(expresion);
        Matcher mat = pat.matcher(addr);
        if (mat.find() == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Valida una data en format dd/mm/aaaa o be en format d/m/aaaa
     *
     * @param data
     * @return
     */
    public static boolean data(String data) {

        boolean ret = false;
        
         Pattern pat = Pattern.compile("^(0?[1-9]|[12][0-9]|3[01])[/](0?[1-9]|1[012])[/]\\d\\d\\d\\d$");
        Matcher mat = pat.matcher(data);
        if (mat.matches()) {
            ret = true;
        }
        
        return ret;
    }

    /**
     * *
     * Valida un codi postal en format NNNNN i amb valors entre 01000 i 52999
     *
     * @param cp
     * @return
     */
    public static boolean cp(String cp) {

        boolean ret = false;
        //String regex = "^(0[1-9]|[1-4][0-9]||5[0-2])[0-9]{3}$"

        return cp.matches("\\d{5}") && Integer.parseInt(cp) >= 1000 && Integer.parseInt(cp) < 53000;
    }

    /**
     * *
     * Retorna una llista d'emails correctes, extrets d'un text amb qualsevol
     * contingut
     *
     * @param s string no null amb qualsevol text
     * @return llista d'emails correctes
     */
    public static List<String> extraureAdrecesEmailCorrectes(String s) {
        List ret = new ArrayList();
        
        Pattern regles = Pattern.compile("[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,}");
        
        //Objecte que processa l'expressió regular i emmagatzema les N coincidencies
        Matcher textAnalitzar = regles.matcher(s);

       
         //Mentre hi hagi coincidencies...
         while (textAnalitzar.find() == true){ 
             
            ret.add(textAnalitzar.group()); 
            // afegim el match a la llista a retornar
             //ret.add(s.substring(textAnalitzar.start(), textAnalitzar.end())); //fa lo mateix que la sentencia group
             
         }
       

        return ret;
    }
}
