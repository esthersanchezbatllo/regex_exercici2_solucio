
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import junit.framework.Assert;
import org.junit.Test;


import utils.Verificador;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author manel
 */
public class Test_cp {
    
    @Test
    public void Test_Regex_cp() {
        
        Random random = new Random();
        NumberFormat formatter = new DecimalFormat("00000");  
                
        List<String> incorrectes = new ArrayList<>(Arrays.asList(
                "1000",
                "-52999",
                "00999",
                "a1000",
                "+1000",
                "12345.",
                "53000",
                "153000"));
        
        // correctes
        System.out.println("CORRECTES:");
        for (int i = 1; i<100; i++)
        {
            Integer n = random.nextInt(51999) + 1000;
            String ok = formatter.format(n);
            System.out.println(ok);
            Assert.assertTrue(Verificador.cp(ok));
        }
        System.out.println();
        
        // incorrectes
        System.out.println("INCORRECTES:");
        for (String s : incorrectes)
        {
            System.out.println(s);
            Assert.assertFalse(Verificador.cp(s));
        }
        System.out.println();
        
    }
    
}
