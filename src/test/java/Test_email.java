
import com.github.javafaker.Faker;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import junit.framework.Assert;
import org.junit.Test;


import utils.Verificador;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author manel
 */
public class Test_email {
    
    //JAVADOC: http://dius.github.io/java-faker/apidocs/index.html
    Faker faker = new Faker();
    
    @Test
    public void Test_Regex_email() {
        
        List<String> incorrectes = new ArrayList<>(Arrays.asList(
                "#@%^%#$@#$@#.com",
                "@example.com",
                "Joe Smith <email@example.com>",
                "email.example.com",
                "email@example@example.com",
                ".email@example.com",
                "email.@example.com",
                "email..email@example.com",
                "email@example.com (Joe Smith)",
                "email@example.com email@example.com",
                "email@111.222.333.44444",
                "email@example..com",
                "Abc..123@example.com"));
        
        // correctes
        System.out.println("CORRECTES:");
        for (int i = 1; i<100; i++)
        {
            String ok = faker.internet().emailAddress();
            System.out.println(ok);
            Assert.assertTrue(Verificador.email(ok));
        }
        System.out.println();
        
        // incorrectes
        System.out.println("INCORRECTES:");
        for (String s : incorrectes)
        {
            System.out.println(s);
            Assert.assertFalse(Verificador.email(s));
        }
        System.out.println();
        
    }
    
}
