
import com.github.javafaker.DateAndTime;
import com.github.javafaker.Faker;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import junit.framework.Assert;
import org.junit.Test;


import utils.Verificador;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author manel
 */
public class Test_extraureAdrecesEmail {
    
    String origen = "For my own and your reference when testing email regexes or other email validation processes, here a copy&paste list of test samples:\n" +
                "\n" +
                "Thanks goes out to Chan Chaiyochlarb who published a major part of this list on the msdn blog, unfortunately it’s in a table there so you can’t just copy & paste them. That’s why I’m saving this here. If you want to know why those emails are valid or invalid, check out his post.\n" +
                "\n" +
                "Stranger valid and invalid email addresses where copied directly from Wikipedia.\n" +
                "\n" +
                "List of Valid Email Addresses\n" +
                "\n" +
                "email@example.com\n" +
                "firstname.lastname@example.com\n" +
                "email@subdomain.example.com\n" +
                "firstname+lastname@example.com\n" +
                "email@123.123.123.123\n" +
                "email@[123.123.123.123]\n" +
                "“email”@example.com\n" +
                "1234567890@example.com\n" +
                "email@example-one.com\n" +
                "_______@example.com\n" +
                "email@example.name\n" +
                "email@example.museum\n" +
                "email@example.co.jp\n" +
                "firstname-lastname@example.com\n" +
                "\n" +
                "List of Strange Valid Email Addresses\n" +
                "\n" +
                "much.“more\\ unusual”@example.com\n" +
                "very.unusual.“@”.unusual.com@example.com\n" +
                "very.“(),:;<>[]”.VERY.“very@\\\\ \"very”.unusual@strange.example.com\n" +
                "\n" +
                "List of Invalid Email Addresses\n" +
                "\n" +
                "plainaddress\n" +
                "#@%^%#$@#$@#.com\n" +
                "@example.com\n" +
                "Joe Smith <email@example.com>\n" +
                "email.example.com\n" +
                "email@example@example.com\n" +
                ".email@example.com\n" +
                "email.@example.com\n" +
                "email..email@example.com\n" +
                "あいうえお@example.com\n" +
                "email@example.com (Joe Smith)\n" +
                "email@example\n" +
                "email@-example.com\n" +
                "email@example.web\n" +
                "email@111.222.333.44444\n" +
                "email@example..com\n" +
                "Abc..123@example.com\n" +
                "\n" +
                "List of Strange Invalid Email Addresses\n" +
                "\n" +
                "“(),:;<>[\\]@example.com\n" +
                "just\"not\"right@example.com\n" +
                "this\\ is\"really\"not\\allowed@example.com\n" +
                "\n" +
                "And there you have it.\n" +
                "\n" +
                "Finally for your pleasure here a php code snippet to test your regex against. Feel free to just copy the array where I have already put in the escaping backslashes and build your own tests around it.\n" +
                "\n" +
                "$emails = array(\n" +
                "    'email@example.com', \n" +
                "    'firstname.lastname@example.com', \n" +
                "    'email@subdomain.example.com', \n" +
                "    'firstname+lastname@example.com', \n" +
                "    'email@123.123.123.123', \n" +
                "    'email@[123.123.123.123]', \n" +
                "    '\"email\"@example.com', \n" +
                "    '1234567890@example.com', \n" +
                "    'email@example-one.com', \n" +
                "    '_______@example.com', \n" +
                "    'email@example.name', \n" +
                "    'email@example.museum', \n" +
                "    'email@example.co.jp', \n" +
                "    'firstname-lastname@example.com', \n" +
                "\n" +
                "    'much.\"more\\ unusual\"@example.com', \n" +
                "    'very.unusual.\"@\".unusual.com@example.com', \n" +
                "    'very.\"(),:;<>[]\".VERY.\"very@\\\\\\\\\\\\ \\\"very\".unusual@strange.example.com', \n" +
                "\n" +
                "\n" +
                "    'plainaddress', \n" +
                "    '#@%^%#$@#$@#.com', \n" +
                "    '@example.com', \n" +
                "    'Joe Smith <email@example.com>', \n" +
                "    'email.example.com', \n" +
                "    'email@example@example.com', \n" +
                "    '.email@example.com', \n" +
                "    'email.@example.com', \n" +
                "    'email..email@example.com', \n" +
                "    'あいうえお@example.com', \n" +
                "    'email@example.com (Joe Smith)', \n" +
                "    'email@example', \n" +
                "    'email@-example.com', \n" +
                "    'email@example.web', \n" +
                "    'email@111.222.333.44444', \n" +
                "    'email@example..com', \n" +
                "    'Abc..123@example.com', \n" +
                "\n" +
                "    '\"(),:;<>[\\]@example.com', \n" +
                "    'just\"not\"right@example.com', \n" +
                "    'this\\ is\\\"really\\\"not\\\\\\\\allowed@example.com'\n" +
                ");\n" +
                "\n" +
                "foreach ($emails as $email) {\n" +
                "    if (preg_match(\"/YOURREGEX/\", $email))\n" +
                "        echo \"<div style='color:green'>\".$email.\" is valid.</div>\";\n" +
                "    else\n" +
                "        echo \"<div style='color:red'>\".$email.\" is invalid.</div>\";\n" +
                "}\n" +
                "PS: Yes, email addresses are a pain in the behind to validate. Wanna see the perfect regex for email addresses? Here it is.\n" +
                "\n" +
                "Wanna see the second best email regex? Here you go: /^.+@.+\\..+$/\n" +
                "It basically says \"There is an at-sign, a period, stuff before/after/between them, and no line breaks.” Of course that’s just my personal opinion.\n" +
                "\n" +
                "If someone wants to spam your form, they will do so regardless of your precious regex. president@whitehouse.gov - There you go. And really, how often have you mistyped your email address and a form validation helped you see it? I know I never have.\n" +
                "\n" +
                "But that list is good for other stuff as well. Right now I’m generating default usernames from the emails people put in. That’s why I made that list to begin with. Anyway, I’m off!";
    
       
    @Test
    public void Test_Regex_extractor() {
        
        Assert.assertTrue(Verificador.extraureAdrecesEmailCorrectes(origen).size() == 47);
        
    }
}
