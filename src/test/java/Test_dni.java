
import com.github.javafaker.Faker;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import junit.framework.Assert;
import org.junit.Test;


import utils.Verificador;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author manel
 */
public class Test_dni {
    
    //JAVADOC: http://dius.github.io/java-faker/apidocs/index.html
    Faker faker = new Faker();
    
    @Test
    public void Test_Regex_dni() {
        
         List<String> incorrectes = new ArrayList<>(Arrays.asList("38044132V",
                                                                "85083745J",
                                                                "22956827P",
                                                                "24860616J",
                                                                "53272037M",
                                                                "00018767T",
                                                                "64025352V",
                                                                "25207335X",
                                                                "56847385P",
                                                                "81356504E",
                                                                "76122621B",
                                                                "14296724E",
                                                                "45644072Y",
                                                                "68265160W",
                                                                "14145049B",
                                                                "34727157N",
                                                                "97603406H",
                                                                "58248379C",
                                                                "18519084C",
                                                                "95355815L",
                                                                "54948066M",
                                                                "20424398X",
                                                                "79643590K",
                                                                "02375791Z",
                                                                "06459717L",
                                                                "53767505J",
                                                                "09013560W",
                                                                "97178144S",
                                                                "91083833L",
                                                                "32010285B",
                                                                "25911641Z",
                                                                "95222835Z",
                                                                "10303387X",
                                                                "08550419A",
                                                                "75799075R",
                                                                "24445268E",
                                                                "0845406P",
                                                                "99013",
                                                                "530681f73",
                                                                "11131.692Z",
                                                                "49321rf540H",
                                                                "59158,201Q",
                                                                "56402&661Z",
                                                                "16662?993E",
                                                                "3939=3153H",
                                                                "2813223978S",
                                                                "670ad59178X",
                                                                "86612340783M",
                                                                "78308823XR",
                                                                "76538939EU"));
        
        List<String> correctes = new ArrayList<>(Arrays.asList("38044133V",
                                                                "85083775J",
                                                                "22936827P",
                                                                "24160616J",
                                                                "53272037N",
                                                                "09018967T",
                                                                "44025352V",
                                                                "28204335X",
                                                                "56897385P",
                                                                "81326504E",
                                                                "76122421B",
                                                                "14296224E",
                                                                "45649072Y",
                                                                "68235160W",
                                                                "14175049B",
                                                                "34727137N",
                                                                "97606406H",
                                                                "58218379C",
                                                                "18919084C",
                                                                "95455815L",
                                                                "54908066M",
                                                                "20424378X",
                                                                "79623590M",
                                                                "02175791Z",
                                                                "06709717L",
                                                                "53792505J",
                                                                "09082564W",
                                                                "97180144S",
                                                                "91082733L",
                                                                "32012285B",
                                                                "25979641Z",
                                                                "95275835Z",
                                                                "10306287X",
                                                                "08790419A",
                                                                "75726075R",
                                                                "24435268E",
                                                                "08457706P",
                                                                "99013517N",
                                                                "53068173C",
                                                                "11131692Z",
                                                                "49321540H",
                                                                "59158201Q",
                                                                "56402661Z",
                                                                "16662993E",
                                                                "39393153H",
                                                                "28123978S",
                                                                "67059178X",
                                                                "86610783M",
                                                                "78308823X",
                                                                "76538939E"));        
        // incorrectes
        System.out.println("INCORRECTES:");
        for (String s : incorrectes)
        {
            System.out.println(s);
            Assert.assertFalse(Verificador.nif(s));
        }
        System.out.println();
        
         // correctes
        System.out.println("CORRECTES:");
        for (String s : correctes)
        {
            System.out.println(s);
            Assert.assertTrue(Verificador.nif(s));
        }
        System.out.println();
        
    }
    
}
