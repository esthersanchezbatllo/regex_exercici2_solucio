
import com.github.javafaker.Faker;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import junit.framework.Assert;
import org.junit.Test;


import utils.Verificador;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author manel
 */
public class Test_Data {
    
    //JAVADOC: http://dius.github.io/java-faker/apidocs/index.html
    Faker faker = new Faker();
    
    @Test
    public void Test_Regex_data() {
        
        String format1 = "d/M/yyyy";
        DateTimeFormatter f1 = DateTimeFormatter.ofPattern(format1);
        
        List<String> incorrectes = new ArrayList<>(Arrays.asList(
                "1-1--2010",
                "41-1-2010",
                "-1-13-2010",
                "-1-1-2010",
                "+1-1-2010",
                "1-1/2010",
                "1/1-2010",
                "1//1/2010",
                "1/1/-2010"));
        
        // correctes
        System.out.println("CORRECTES:");
        for (int i = 1; i<100; i++)
        {
            Date ok = faker.date().birthday();
            LocalDate lok = LocalDate.ofInstant(ok.toInstant(), ZoneId.systemDefault());
            String data = f1.format(lok);
            System.out.println(data);
            Assert.assertTrue(Verificador.data(data));
        }
        System.out.println();
        
        // incorrectes
        System.out.println("INCORRECTES:");
        for (String s : incorrectes)
        {
            System.out.println(s);
            Assert.assertFalse(Verificador.data(s));
        }
        System.out.println();   
    }
}
